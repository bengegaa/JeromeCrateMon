from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('myform.views',#'myform2.views',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^mysite/', include('mysite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
      url(r'^contact$', 'contact', name='contact'),
      url(r'^contact/$', 'contact', name='contact'),
      url(r'^contact/contact.html$', 'contact', name='contact'),
      url(r'^success$', 'success', name='success'),
      url(r'^glib$', 'contact', name='contact'),
      url(r'^glib/$', 'contact', name='contact'),
)
urlpatterns += patterns('myform2.views',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^mysite/', include('mysite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
      url(r'^successscan$', 'successscan', name='successscan'),
      url(r'^successpower$', 'successpower', name='successpower'),
      url(r'^glibpower/$', 'contact2', name='contact2'),
      url(r'^glibpower$', 'contact2', name='contact2'),
)
