# Create your views here.
from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse, RequestContext
from django import forms
import uhal
import commands
import csv
import time
import binascii

global natmch
global regvalue
global manager2
global hw2
global connect

class ContactForm2(forms.Form):
    global manager2
    global hw2
    natmch = forms.IPAddressField()
    connect = forms.CharField()
    regvalue = forms.CharField()

class myForm(forms.Form):
    global manager2
    global hw2
    myip = forms.IPAddressField()

def contact2(request):
        global natmch
        global regvalue
        global toto 
        global manager2
        global hw2
	global connect  
        global myip      
	toto="JEROME" 
        if request.method=='POST':
	   form1 = myForm(request.POST)
	   if form1.is_valid(): # All validation rules pass 
              myip = form1.cleaned_data['myip']
              return HttpResponseRedirect('/successscan') # Redirect after POST
        else:
           form1= myForm()
	if request.method == 'POST': # If the form has been submitted...
	    form = ContactForm2(request.POST) # A form bound to the POST data
            if form.is_valid(): # All validation rules pass 
               natmch = form.cleaned_data['natmch']
               connect = form.cleaned_data['connect']
               regvalue = form.cleaned_data['regvalue']
	       return HttpResponseRedirect('/successpower') # Redirect after POST 
	else:	
	    form = ContactForm2()
            
	return render_to_response('contact2.html', {'form': form, }, context_instance=RequestContext(request))


def successscan(request):
   global myip
   message2 = "<html><body><head><title>SCAN IP:"+str(myip)+"</title>"
   message2 += '<link href="/static/css/style.css" rel="stylesheet" type="text/css" media="screen">'
   message2 +="</head><center><h1>SCAN IP:"+str(myip)+"</h1>"
   cmdping ='''ping -c 1 -W 1 '''
   cmdping +=str(myip)
   #cmdping=str(cmdping)
   #pres_card = os.system(cmdping)
   pres_card,outputcmd = commands.getstatusoutput(cmdping)
   if pres_card==0:
    message2 += '<div id="valid">PING OK on IP:'+str(myip)+'</div><br>'
    cmdshell='''python /home/xtaldaq/mysite/pingnetwork2html.py ''' + str(myip)
    commands.getstatusoutput(cmdshell)
   else:
    message2 += '<div id="error">PING NOT OK on IP:'+str(myip)+'</div><br>'
    cmdshell='''python /home/xtaldaq/mysite/pingnetwork2html.py ''' + str(myip)
    commands.getstatusoutput(cmdshell)
   message2 += "<br><a href=\"glibpower\" id=\"retour\" align='center'>Retour Home</a><div id='sign' align='center'>Created in 2013 by J.HOSSELET IPHC/CNRS/IN2P3</div></body></html>"
   message2 +="</body></html>"
   return HttpResponse(message2)



def successpower(request):
   global natmch
   global addresse
   global regvalue
   global manager2
   global hw2
   global connect
   message = "<html><body><head><title>POWER MANAGEMENT SUR GLIB:</title>"
   message+= '<link href="/static/css/style.css" rel="stylesheet" type="text/css" media="screen">'
   message += "<script>function visibilite(thingId) { "
   message += "var targetElement; targetElement = document.getElementById(thingId) ; "
   message += "if (targetElement.style.display == \"none\") {  targetElement.style.display = \"\" ;} else { " 
   message += "targetElement.style.display = \"none\" ; "
   message += "} } </script></head>"
   message +="<center><h1>POWER MANAGEMENT SUR GLIB:</h1><div id=\"info\">"
   message += "GLIBSLOT"+str(connect) +" via NAT-MCH IP:"+str(natmch) +"</div></center><br>"
   message += "<br><a href='' id=\"menu\" onclick=\"javascript:visibilite('id_div_eglib');return false;\">Envoi GLIB</a>"
   message += " | <a href='' id=\"menu\" onclick=\"javascript:visibilite('id_div_rglib');return false;\">Récupération GLIB</a>"
   message += " | <a href='' id=\"menu\" onclick=\"javascript:visibilite('id_div_statTCA');return false;\">Statistique &micro;TCA (IP: "+str(natmch)+")</a>"
   message += "<div id=\"id_div_eglib\" style=\"display:none;\">"
   message += "<a href='' id=\"masque\" onclick=\"javascript:visibilite('id_div_eglib');return false;\"><h3>Envoi GLIB</h3></a>"
   message +="<br>IP NAT-MCH="+ str(natmch)
   message +="<br>Connection SLOT="+ str(connect)
   #message +="<br>Adresse register="+ str(addresse)
   message +='<div id="value">Register value='+ str(regvalue) +'</div></div>'
   
   manager2 = uhal.ConnectionManager("file://~/xml/connections.xml")
   for i in (range(0,13)):
      if i>0:
       strconnect="GLIB_SLOT"+str(i)
       hw2 = manager2.getDevice(strconnect)
      else:
       strconnect="GLIBboardfake"
       hw2 = manager2.getDevice(strconnect)
      ip=hw2.uri()
      ip=ip[15::]
      ip=ip[:15:]
      #ip='127.000.000.001' 
      cmdping='ping -c 1 -W 1 '+ str(ip)
      pres_glib,outputcmd = commands.getstatusoutput(cmdping)
      if pres_glib==0: 
       if i==0:
	message += '<div id="valid_stat">GLIBfakeboard IS CONNECTED to DummyHardware :-) on ' + ip + '</div>'
       else:
        message += '<div id="valid_stat">GLIB IS CONNECTED  :-) on crate uTCA with IP:' + ip +' on SLOT'+str(i)+'</div>'
      else:
       message += '<div id="error_stat">GLIB IS NOT CONNECTED  :-( on crate uTCA with IP:'+ ip +'</div>'	

   #hw = manager.getDevice("GLIBboardfake")
   #hw = manager.getDevice("GLIBboard")
   #hw = manager.getDevice("localhost")	
   #monnode = str(addresse)
   #mystr=monnode.split(".")
   #mylen=len(mystr)
   mylen=1
   node="TOTOT"
   perm="READ"
   message += "<div id=\"id_div_rglib\" style=\"display:none;\">"
   message += "<a href='' id=\"masque\" onclick=\"javascript:visibilite('id_div_rglib');return false;\"><h3>Récupération GLIB:</h3></a>"
   #message += "<br>mylen="+str(mylen)
   #message += "<br>mystr="+str(mystr)
   
  # Send IPbus transactions
   if pres_glib==0: 
    #hw2.dispatch()
    message += "<div id=\"valid\">Recuperation success</div></div>"
   else:
    message += "<div id=\"error\">Aucune Recuperation</div></div>"
  # Print the register value to screen as a decimal:
    #message += '<div id="value">Registre value relecture :'+reg+"</div>"
    
   cmdping='ping -c 1 -W 1 '+str(natmch)
   x,outputcmd = commands.getstatusoutput(cmdping)
   #x=0 # FORCAGE VALEUR
   if x==0: 
    message += '<div id="valid_stat">NAT-MCH IS CONNECTED  :-) on ' + str(natmch)+'</div>'
   else:
    message += '<div id="error_stat">NAT-MCH IS NOT CONNECTED  :-( on ' + str(natmch)+'</div>'
    message += "<div id=\"id_div_statTCA\" style=\"display:none;\">"
    message += ""
    message += "<a href='' id=\"masque\" onclick=\"javascript:visibilite('id_div_statTCA');return false;\"><h3>Statistique &micro;TCA</h3></a>"
   if x==0:
    if connect != 'GLIBfakeboard':
     channel=int(connect)+4
     if regvalue =='ON':
      cmdipmi="ipmitool -I lan -H "+str(natmch) +" -P '' -T 0x82 -B 0 -t 0xC2 -b 7 raw 44 0x24 0 "+str(channel)+" 5 0 1 0"
     if regvalue =='OFF':
      cmdipmi="ipmitool  -I lan -H "+str(natmch) +" -P '' -T 0x82 -B 0 -t 0xC2 -b 7 raw 44 0x24 0 "+str(channel)+" 4 0 1 0"
    if connect != 'GLIBfakeboard':
     res=commands.getstatusoutput(cmdipmi)	
    message += "<div>AFTER COMMANDE</div>"
    time.sleep(1)
    cmdipmi="ipmitool -H "+str(natmch) +" -P '' -c sdr > /home/xtaldaq/mysite/myform/output.csv"
    #res=commands.getstatusoutput(cmdipmi)
    time.sleep(1)
    cr = csv.reader(open("/home/xtaldaq/mysite/myform/output.csv","r"))
    #message = "<table  BORDER='1'><tr><th>ID</th><th>ID2</th><th>ID3</th></tr>" 
    dt=time.strftime('%d/%m/%y %H:%M',time.localtime())
    for i in (range(0,13)):
       if i>0:
        strconnect="GLIB_SLOT"+str(i)
        hw2 = manager2.getDevice(strconnect)
       else:
        strconnect="GLIBboardfake"
        hw2 = manager2.getDevice(strconnect)
       ip=hw2.uri()
       ip=ip[15::]
       ip=ip[:15:]
       #ip='127.000.000.001' 
       cmdping="ping -c 1 -W 1 "+ str(ip)
       pres_glib,outputcmd = commands.getstatusoutput(cmdping)
       if pres_glib==0: 
        if i==0:
	 message += '<div id="valid_stat">GLIBfakeboard IS CONNECTED to DummyHardware :-) on ' + ip + '</div>'
        else:
         message += '<div id="valid_stat">GLIB IS CONNECTED  :-) on crate uTCA with IP:' + ip +' on SLOT'+str(i)+'</div>'
       else:
        message += '<div id="error_stat">GLIB IS NOT CONNECTED  :-( on crate uTCA with IP:'+ ip +'</div>'	
    cmdping="ping -c 1 -W 1 "+str(natmch)
    x ,outputcmd = commands.getstatusoutput(cmdping)
    #x=0 # FORCAGE VALEUR
   if x==0: 
    message += '<div id="valid_stat">NAT-MCH IS CONNECTED  :-) on ' + str(natmch)+'</div>'
    message += "<div id=\"id_div_statTCA\" style=\"display:none;\">"
    message += ""
    message += "<a href='' id=\"masque\" onclick=\"javascript:visibilite('id_div_statTCA');return false;\"><h3>Statistique &micro;TCA</h3></a>"
    message += "<br><center><bold>CRATE GLIB via NAT-MCH IP="+ str(natmch) +"</bold><div id=\"valid\"> Maj:"+ dt +" </div><table><tr><th>Sensor</th><th>Valeur</th><th>Etat/Unité</th></tr>"
    for row in cr:
    #print row
     message += "<tr>"
     message += "<td>"+ row[0] + "</td><td>"+ row[1]+"</td><td>"+row[2]+"</td></tr>"
     #message += "<td>"+ row[0] + "</td><td></tr>"
    message += '</table></center></div><br>'
   else:
    message += '<div id="error_stat">NAT-MCH IS NOT CONNECTED  :-( on ' + str(natmch)+'</div>'
    message += '<div id="error">NO STATISTIC FROM CRATE : NAT-MCH is not connected or NAT-MCH IP is not correct.'
    message += 'Check your network link or change NAT-MCH IP for known a statistics</div></div>'
    message += ''
   message += "<br><a href=\"glibpower/\" id=\"retour\" align='center'>Retour Home</a><div id='sign' align='center'>Created in 2013 by J.HOSSELET IPHC/CNRS/IN2P3</div></body></html>"
   return HttpResponse(message)

