from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse, RequestContext
from django import forms
import uhal
import commands
import csv
import time
import binascii
import os

global natmch
global addresse
global regvalue
global manager
global hw
global connect

class ContactForm(forms.Form):
    global hw
    global manager
    natmch = forms.IPAddressField()
    connect = forms.CharField(max_length=100)
    addresse = forms.CharField(max_length=100)
    regvalue = forms.IntegerField()
    cc_myself = forms.BooleanField(required=False)

def contact(request):
        global natmch
        global addresse
        global regvalue
        global toto 
	global manager
	global hw
	global connect        
	toto="JEROME" 
	if request.method == 'POST': # If the form has been submitted...
	    form = ContactForm(request.POST) # A form bound to the POST data
            if form.is_valid(): # All validation rules pass 
               natmch = form.cleaned_data['natmch']
               connect = form.cleaned_data['connect']
               addresse = form.cleaned_data['addresse']
               regvalue = form.cleaned_data['regvalue']
               cc_myself = form.cleaned_data['cc_myself']
	       return HttpResponseRedirect('/success') # Redirect after POST
	else:	
	    form = ContactForm()

	return render_to_response('contact.html', {'form': form, }, context_instance=RequestContext(request))

def success(request):
   global natmch
   global addresse
   global regvalue
   global manager
   global hw
   global connect
   message = "<html><body><head><title>ENVOI ET RECUPERATION SUR GLIB:</title>"
   message+= '<link href="/static/css/style.css" rel="stylesheet" type="text/css" media="screen">'
   message += "<script>function visibilite(thingId) { "
   message += "var targetElement; targetElement = document.getElementById(thingId) ; "
   message += "if (targetElement.style.display == \"none\") {  targetElement.style.display = \"\" ;} else { " 
   message += "targetElement.style.display = \"none\" ; "
   message += "} } </script></head>"
   message +="<center><h1>ENVOI ET RECUPERATION SUR GLIB:</h1><div id=\"info\">"
   message += str(connect) +" via NAT-MCH IP:"+str(natmch) +"</div></center><br>"
   message += "<br><a href='' id=\"menu\" onclick=\"javascript:visibilite('id_div_eglib');return false;\">Envoi GLIB</a>"
   message += " | <a href='' id=\"menu\" onclick=\"javascript:visibilite('id_div_rglib');return false;\">Récupération GLIB</a>"
   message += " | <a href='' id=\"menu\" onclick=\"javascript:visibilite('id_div_statTCA');return false;\">Statistique &micro;TCA (IP: "+str(natmch)+")</a>"
   message += "<div id=\"id_div_eglib\" style=\"display:none;\">"
   message += "<a href='' id=\"masque\" onclick=\"javascript:visibilite('id_div_eglib');return false;\"><h3>Envoi GLIB</h3></a>"
   message +="<br>IP NAT-MCH="+ str(natmch)
   message +="<br>Connection SLOT="+ str(connect)
   message +="<br>Adresse register="+ str(addresse)
   message +='<div id="value">Register value='+ str(regvalue) +'</div></div>'
   
   manager = uhal.ConnectionManager("file://~/xml/connections.xml")
   hw = manager.getDevice(str(connect))
   ip=hw.uri()
   ip=ip[15::]
   ip=ip[:15:]
   cmdping="ping -c 1 -W 1 "+ ip
   pres_glib = os.system(cmdping)
   if pres_glib==0: 
    message += '<div id="valid_stat">GLIB IS CONNECTED  :-) on ' + ip + '</div>'
   else:
    message += '<div id="error_stat">GLIB IS NOT CONNECTED  :-( on '+ ip +'</div>'	
   #hw = manager.getDevice("GLIBboardfake")
   #hw = manager.getDevice("GLIBboard")
   #hw = manager.getDevice("localhost")	
   monnode = str(addresse)
   mystr=monnode.split(".")
   mylen=len(mystr)
   node="TOTOT"
   perm="READ"
   message += "<div id=\"id_div_rglib\" style=\"display:none;\">"
   message += "<a href='' id=\"masque\" onclick=\"javascript:visibilite('id_div_rglib');return false;\"><h3>Récupération GLIB:</h3></a>"
   #message += "<br>mylen="+str(mylen)
   #message += "<br>mystr="+str(mystr)
   if mylen == 1:
    node= hw.getNode(mystr[0])
    perm =str(hw.getNode(mystr[0]).getPermission())
   elif mylen == 2:
    node= hw.getNode(mystr[0]).getNode(mystr[1])
    perm =str(hw.getNode(mystr[0]).getNode(mystr[1]).getPermission())
   elif mylen == 3:
    node= hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2])
    perm =str(hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2]).getPermission())  
   if perm == 'READ':
    message += "<br>Register READ ONLY  :do nothing"
   elif perm == 'READWRITE':
    message += "<br>Register READWRITE  : write ok"
    if mylen == 1:
     node= hw.getNode(mystr[0])
     #hw.getNode(mystr[0]).write(regvalue)
     message +='<br><div id="error">NOT YET IMPLEMENTED : NOT Working at this time</div>'
    elif mylen == 2:
     node= hw.getNode(mystr[0]).getNode(mystr[1])
     ##hw.getNode(mystr[0]).getNode(mystr[1]).write(regvalue)
     #node.write(regvalue)
     message +='<br><div id="error">NOT YET IMPLEMENTED : NOT Working at this time</div>'
    elif mylen == 3:
     node= hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2])
     message +='<br><div id="error">NOT YET IMPLEMENTED : NOT Working at this time</div>'
     #node.write(regvalue)
     #hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2]).write(regvalue)
   elif perm == 'WRITE':
    message += "<br>WRITE ONLY"
    if mylen == 1:
     node= hw.getNode(mystr[0])
     hw.getNode(mystr[0]).write(regvalue)
    elif mylen == 2:
     node= hw.getNode(mystr[0]).getNode(mystr[1])
     node.write(regvalue)
     #hw.getNode(mystr[0]).getNode(mystr[1]).write(regvalue)
    elif mylen == 3:
     node= hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2])
     node.write(regvalue)
     #hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2]).write(regvalue)
    #node.write(regvalue)
   
  # Read the value back.
  # NB: the reg variable below is a uHAL "ValWord", not just a simple integer
   if perm == 'READ':
    message += "<br>for read : READ ok"
    if mylen == 1:
     node= hw.getNode(mystr[0])
     reg=hw.getNode(mystr[0]).read()
    elif mylen == 2:
     node= hw.getNode(mystr[0]).getNode(mystr[1])
     #reg=hw.getNode(mystr[0]).getNode(mystr[1]).read()
     reg=node.read() 
    elif mylen == 3:
     node= hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2])
     #reg=hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2]).read()
     reg=node.read()
   elif perm == 'READWRITE':
    message += "<br>for read : READWRITE ok"
    if mylen == 1:
     node= hw.getNode(mystr[0])
     reg=hw.getNode(mystr[0]).read()
    elif mylen == 2:
     node= hw.getNode(mystr[0]).getNode(mystr[1])
     #reg=hw.getNode(mystr[0]).getNode(mystr[1]).read()
     reg=node.read()
    elif mylen == 3:
     node= hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2])
     #reg=hw.getNode(mystr[0]).getNode(mystr[1]).getNode(mystr[2]).read()   
     reg= node.read() 	
  # Send IPbus transactions
   if pres_glib==0: 
    hw.dispatch()
    message += "<div id=\"valid\">Recuperation success</div>"
  # Print the register value to screen as a decimal:
  #print " REG =", reg
    message += "<br>Id : "+str(node.getId())
    message += "<br>Id entiere: "+str(monnode)
    message += '<div id="value">Registre value relecture (en hexa) :'+hex(reg)+"</div>"
    message += '<div id="value">Registre value relecture (en decimal) :'+str(reg.value())+"</div>"
    toto=hex(reg)
    print toto	
    if toto != '0x0':
     if len(toto) > 4:
      print toto[2::].decode('hex')
      message += "<div id=\"value\">Registre value relecture (en ASCII) :"+ toto[2::].decode('hex') +"</div>"
    message += "<br>Addr : "+hex(node.getAddress())
    message += "<br>Mask : "+hex(node.getMask()) 
    message += "<br>Mode : "+ str(node.getMode())   # Mode enum - one of uhal.BlockReadWriteMode.SINGLE (default), INCREMENTAL and NON_INCREMENTAL, or HIERARCHICAL for top-level nodes nesting other nodes.
    message += "<br>Read/write permissions : "+ str(node.getPermission())  # One of uhal.NodePermission.READ, WRITE and READWRITE
    message += "<br>Size (in units of 32-bits) : "+str(node.getSize())     # In units of 32-bits. All single registers and FIFOs have default size of 1
    message += "<br>Tags : "+node.getTags()  # User-definable string from address table - in principle a comma separated list of values	
    message += "</div>"
   else:
    message += '<br><div id="error">Dispatch NOT OK => GLIB NOT PRESENT</div></div>'
  # Print the register value to screen in hex:
  #print " REG =", hex(reg)
  # Get the underlying integer value from the reg "ValWord"
  #value = int(reg) # Equivalent to reg.value()
   cmdping="ping -c 1 -W 1 "+natmch
   x = os.system(cmdping)
   #x=0 # FORCAGE VALEUR
   if x==0: 
    message += '<div id="valid_stat">NAT-MCH IS CONNECTED  :-) on ' + str(natmch)+'</div>'
   else:
    message += '<div id="error_stat">NAT-MCH IS NOT CONNECTED  :-( on ' + str(natmch)+'</div>'
    message += "<div id=\"id_div_statTCA\" style=\"display:none;\">"
    message += ""
    message += "<a href='' id=\"masque\" onclick=\"javascript:visibilite('id_div_statTCA');return false;\"><h3>Statistique &micro;TCA</h3></a>"
   if x==0:
    cmdipmi="ipmitool -H "+str(natmch) +" -P '' -c sdr > /home/xtaldaq/mysite/myform/output.csv"
    res=commands.getstatusoutput(cmdipmi)
    time.sleep(1)
    cr = csv.reader(open("/home/xtaldaq/mysite/myform/output.csv","r"))
    #message = "<table  BORDER='1'><tr><th>ID</th><th>ID2</th><th>ID3</th></tr>" 
    dt=time.strftime('%d/%m/%y %H:%M',time.localtime()) 
    message += "<div id=\"id_div_statTCA\" style=\"display:none;\">"
    message += ""
    message += "<a href='' id=\"masque\" onclick=\"javascript:visibilite('id_div_statTCA');return false;\"><h3>Statistique &micro;TCA</h3></a>"
    message += "<br><center><bold>CRATE GLIB via NAT-MCH IP="+ str(natmch) +"</bold><div id=\"valid\"> Maj:"+ dt +" </div><table><tr><th>Sensor</th><th>Valeur</th><th>Etat/Unité</th></tr>"
    for row in cr:
    #print row
     message += "<tr>"
     message += "<td>"+ row[0] + "</td><td>"+ row[1]+"</td><td>"+row[2]+"</td></tr>"
     #message += "<td>"+ row[0] + "</td><td></tr>"
    message += '</table></center></div><br>'
   else:
    message += '<div id="error">NO STATISTIC FROM CRATE : NAT-MCH is not connected or NAT-MCH IP is not correct.'
    message += 'Check your network link or change NAT-MCH IP for known a statistics</div></div>'
    message += ''
   message += "<br><a href=\"javascript:history.go(-1);\" id=\"retour\" align='center'>Retour Home</a><div id='sign' align='center'>Created in 2013 by Jérôme HOSSELET IPHC/CNRS/IN2P3</div></body></html>"
   return HttpResponse(message)


